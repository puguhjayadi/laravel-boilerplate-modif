<?php

namespace App\Repositories\Backend\Auth;

use App\Events\Backend\Auth\Permission\PermissionCreated;
use App\Events\Backend\Auth\Permission\PermissionUpdated;
use App\Events\Backend\Auth\Permission\PermissionDeleted;
use App\Events\Backend\Auth\Permission\PermissionRestored;
use App\Exceptions\GeneralException;
use App\Models\Auth\Permission;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class PermissionRepository.
 */
class PermissionRepository extends BaseRepository
{
    /**
     * PermissionRepository constructor.
     *
     * @param  Permission  $model
     */
    public function __construct(Permission $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $data
     *
     * @throws GeneralException
     * @throws \Throwable
     * @return Permission
     */
    public function create(array $data): Permission
    {
        // Make sure it doesn't already exist
        if ($this->permissionExists($data['name'])) {
            throw new GeneralException('A permission already exists with the name '.e($data['name']));
        }

        return DB::transaction(function () use ($data) {
            $permission = $this->model::create(['name' => strtolower($data['name'])]);

            if ($permission) {

                event(new PermissionCreated($permission));

                return $permission;
            }

            throw new GeneralException(trans('exceptions.backend.access.permissions.create_error'));
        });
    }


        /**
     * @param Permission  $permission
     * @param array $data
     *
     * @throws GeneralException
     * @throws \Throwable
     * @return mixed
     */
        public function update(Permission $permission, array $data)
        {
            if ($permission->isAdmin()) {
                throw new GeneralException('You can not edit the administrator permission.');
            }

        // If the name is changing make sure it doesn't already exist
            if ($permission->name !== strtolower($data['name'])) {
                if ($this->permissionExists($data['name'])) {
                    throw new GeneralException('A permission already exists with the name '.$data['name']);
                }
            }

            return DB::transaction(function () use ($permission, $data) {
                if ($permission->update([
                    'name' => strtolower($data['name']),
                ])) {

                    event(new PermissionUpdated($permission));

                    return $permission;
                }

                throw new GeneralException(trans('exceptions.backend.access.permissions.update_error'));
            });
        }

        /**
     * @param Permission $permission
     *
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     * @return Permission
     */
    public function delete(Permission $permission) : Permission
    {

        return DB::transaction(function () use ($permission) {
            $delete = $permission->delete();

            if ($delete) {

                event(new PermissionDeleted($permission));

                return $permission;
            }

            throw new GeneralException(__('exceptions.backend.access.permissions.delete_error'));
        });
    }

    /**
     * @param $name
     *
     * @return bool
     */
    protected function permissionExists($name): bool
    {
        return $this->model
        ->where('name', strtolower($name))
        ->count() > 0;
    }

}
