<?php

namespace App\Http\Controllers\Backend\Auth\Permission;

use App\Events\Backend\Auth\Permission\PermissionDeleted;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Auth\Permission\ManagePermissionRequest;
use App\Http\Requests\Backend\Auth\Permission\StorePermissionRequest;
use App\Http\Requests\Backend\Auth\Permission\UpdatePermissionRequest;
use App\Models\Auth\Permission;
use App\Repositories\Backend\Auth\PermissionRepository;

/**
 * Class PermissionController.
 */
class PermissionController extends Controller
{
    /**
     * @var PermissionRepository
     */
    protected $permissionRepository;

    /**
     * @param PermissionRepository $permissionRepository
     */
    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * @param ManagePermissionRequest $request
     *
     * @return mixed
     */
    public function index(ManagePermissionRequest $request)
    {
        return view('backend.auth.permission.index')
        ->withPermissions($this->permissionRepository
            ->with('permissions')
            ->orderBy('id')
            ->paginate());
    }

    /**
     * @param ManagePermissionRequest $request
     *
     * @return mixed
     */
    public function create(ManagePermissionRequest $request)
    {
        return view('backend.auth.permission.create')
        ->withPermissions($this->permissionRepository->get());
    }

    /**
     * @param  StorePermissionRequest  $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StorePermissionRequest $request)
    {
        $this->permissionRepository->create($request->only('name'));
        return redirect()->route('admin.auth.permission.index')->withFlashSuccess(__('alerts.backend.permissions.created'));
    }

    /**
     * @param ManagePermissionRequest $request
     * @param Permission              $permission
     *
     * @return mixed
     */
    public function edit(ManagePermissionRequest $request, Permission $permission)
    {
        if ($permission->isAdmin()) {
            return redirect()->route('admin.auth.permission.index')->withFlashDanger('You can not edit the administrator permission.');
        }

        return view('backend.auth.permission.edit')
        ->withPermission($permission);
    }

    /**
     * @param  UpdatePermissionRequest  $request
     * @param  Permission  $permission
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdatePermissionRequest $request, Permission $permission)
    {
        $this->permissionRepository->update($permission, $request->only('name'));

        return redirect()->route('admin.auth.permission.index')->withFlashSuccess(__('alerts.backend.permissions.updated'));
    }

    /**
     * @param ManagePermissionRequest $request
     * @param Permission              $permission
     *
     * @throws \Exception
     * @return mixed
     */
    public function destroy(ManagePermissionRequest $request, Permission $permission)
    {
        if ($permission->isAdmin()) {
            return redirect()->route('admin.auth.permission.index')->withFlashDanger(__('exceptions.backend.access.permissions.cant_delete_admin'));
        }

        $this->permissionRepository->deleteById($permission->id);

        event(new PermissionDeleted($permission));

        return redirect()->route('admin.auth.permission.index')->withFlashSuccess(__('alerts.backend.permissions.deleted'));
    }
    
}
