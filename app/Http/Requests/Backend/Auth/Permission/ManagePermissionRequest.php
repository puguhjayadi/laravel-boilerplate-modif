<?php

namespace App\Http\Requests\Backend\Auth\Permission;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ManagePermissionRequest.
 */
class ManagePermissionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
